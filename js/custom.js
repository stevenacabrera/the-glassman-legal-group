$("#header-navigation-bars").click(function() {
  $("html").toggleClass("menu-active");
  $("body").toggleClass("menu-active");
  $(this).toggleClass("header-navigation-close");
  $("#header-navigation-main").toggle("slide", {
    direction: "up"
  }, 400);
});

$(window).scroll(function() {
  if ($(window).scrollTop() > 150) {
    $("#header-navigation h1").addClass("scroll-down");
    $("#header-navigation-main").addClass("scroll-down");
  } else {
    $("#header-navigation h1").removeClass("scroll-down");
    $("#header-navigation-main").removeClass("scroll-down");
  }
});

$("#header-navigation-contact-email").click(function() {
if ($(window).width() < 768) {
  $('html,body').animate({
    scrollTop: $("#contact").offset().top - 60
  }, 500);
} else {
  $('html,body').animate({
    scrollTop: $("#contact").offset().top - 80
  }, 500);
}
  $('#first_name').focus();
});
